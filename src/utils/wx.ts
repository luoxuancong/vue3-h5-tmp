//关闭微信页面
export const weixinClosePage = () => {
    if (typeof WeixinJSBridge == 'undefined') {
        if (document.addEventListener) {
            document.addEventListener('WeixinJSBridgeReady', weixin_ClosePage, false);
        } else if (document.attachEvent) {
            document.attachEvent('WeixinJSBridgeReady', weixin_ClosePage);
            document.attachEvent('onWeixinJSBridgeReady', weixin_ClosePage);
        }
    } else {
        weixin_ClosePage();
    }
};
const weixin_ClosePage = () => {
    WeixinJSBridge.call('closeWindow');
};
