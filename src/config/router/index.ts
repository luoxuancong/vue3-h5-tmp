/*
 * @Descripttion:首页 router配置文件
 * @version: 1.0.0
 * @Author: zhoukai
 * @Date: 2022-10-18 11:03:13
 * @LastEditors: zhoukai
 * @LastEditTime: 2022-10-20 14:14:21
 */

const routes = [
    {
        path: '/',
        redirect: '/home'
    },
    {
        path: '/home',
        name: 'home',
        meta: {
            title: '菜单'
        },
        component: () => import('@/views/index.vue')
    },
    {
        path: '/index',
        name: 'index',
        meta: {
            title: '首页',
            keepAlive: false
        },
        // 路由级代码拆分
        // 这会为此路由生成一个单独的块 (index.[hash].js)
        // 访问路由时延迟加载。
        component: () => import('@/views/index/index.vue')
    },
    {
        path: '/equity',
        name: 'equity',
        meta: {
            title: '权益',
            keepAlive: false
        },
        // 路由级代码拆分
        // 这会为此路由生成一个单独的块 (index.[hash].js)
        // 访问路由时延迟加载。
        component: () => import('@/views/equity/equity.vue')
    },
    {
        path: '/share',
        name: 'share',
        meta: {
            title: '分享平台',
            keepAlive: false
        },
        // 路由级代码拆分
        // 这会为此路由生成一个单独的块 (index.[hash].js)
        // 访问路由时延迟加载。
        component: () => import('@/views/share/share.vue')
    },
    {
        path: '/docEquity',
        name: 'docEquity',
        meta: {
            title: '视频医生权益明细',
            keepAlive: false
        },
        // 路由级代码拆分
        // 这会为此路由生成一个单独的块 (index.[hash].js)
        // 访问路由时延迟加载。
        component: () => import('@/views/docEquity/docEquity.vue')
    },
    {
        path: '/aiEquity',
        name: 'aiEquity',
        meta: {
            title: 'Ai视频医生权益明细',
            keepAlive: false
        },
        // 路由级代码拆分
        // 这会为此路由生成一个单独的块 (index.[hash].js)
        // 访问路由时延迟加载。
        component: () => import('@/views/aiEquity/aiEquity.vue')
    },
    {
        path: '/video-doc',
        name: 'videoDoc',
        meta: {
            title: '视频医生',
            keepAlive: false
        },
        component: () => import('@/views/videoDoctor/videoDoctor.vue')
    },
    {
        path: '/user',
        name: 'user',
        meta: {
            title: '个人资料',
            keepAlive: false
        },
        // 路由级代码拆分
        // 这会为此路由生成一个单独的块 (index.[hash].js)
        // 访问路由时延迟加载。
        component: () => import('@/views/user/user.vue')
    },
    {
        path: '/addr',
        name: 'addr',
        meta: {
            title: '我的地址',
            keepAlive: false
        },
        // 路由级代码拆分
        // 这会为此路由生成一个单独的块 (index.[hash].js)
        // 访问路由时延迟加载。
        component: () => import('@/views/addr/addr.vue')
    },
    {
        path: '/addr-edit',
        name: 'edit',
        meta: {
            title: '编辑地址',
            keepAlive: false
        },
        component: () => import('@/views/addr/edit.vue')
    },
    {
        path: '/bind-phone',
        name: 'bindPhone',
        meta: {
            title: '绑定手机号',
            keepAlive: false
        },
        component: () => import('@/views/bindPhone/bindPhone.vue')
    }
];
export default routes;
