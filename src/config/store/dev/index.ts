/*
 * @Descripttion: pinia切片文件-dev
 * @version: 1.0.0
 * @Author: zhoukai
 * @Date: 2022-11-28 12:32:36
 * @LastEditors: zhoukai
 * @LastEditTime: 2023-02-02 17:02:10
 */

// state的接口定义，更好的Typescript体验。
import type { StateValidator, userInfoValidator } from './validation';
import { useCookies } from 'vue3-cookies';
const { cookies } = useCookies();
import type { getCustomerValidator } from '@/config/apis/dev/validation/api';

import { getCustomer } from '@/config/apis/dev/api';
/**
 * 创建 Store
 * 你可以对 Store进行任意命名，但最好使用 store 的名字，同时以 `use` 开头且以 `Store` 结尾。
 * (比如 `useUserStore`，`useCartStore`，`useProductStore`)
 */
export const useDevStore = defineStore({
    id: 'dev', //这个是你的应用中 Store 的唯一 ID。
    // 推荐使用 完整类型推断的箭头函数
    // state属性
    state: (): StateValidator => {
        return {
            count: 0
        };
    },
    // getters
    getters: {
        getCountData: (state) => {
            return '获取到count' + state.count;
        }
    },
    // actions
    actions: {
        saveCountData(count: number) {
            this.count = count;
        }
    }
});
interface userType {
    userinfo: userInfoValidator;
}
export const useUserStore = defineStore({
    id: 'userInfo', //这个是你的应用中 Store 的唯一 ID。
    // 推荐使用 完整类型推断的箭头函数
    // state属性
    state: (): userType => {
        return {
            userinfo: {
                id: '',
                openid: '',
                userName: '',
                avatar: '',
                age: 0,
                grade: '',
                sex: '',
                phone: '',
                souceId: '',
                sourceType: '',
                acquisitionNum: '',
                accessToken: ''
            }
        };
    },
    // getters
    getters: {
        getUserData: (state) => {
            return state.userinfo;
        },
        getAccessToken: (state) => {
            return state.userinfo.accessToken;
        }
    },
    // actions
    actions: {
        async getUerInfo(): Promise<any> {
            return new Promise<void>((resolve) => {
                const openId = cookies.get('openId');
                const openIdUserInfo = cookies.get(openId + '_data');
                if (openId && openIdUserInfo) {
                    console.log('读取缓存成功',openIdUserInfo);
                    this.userinfo = openIdUserInfo;
                    resolve();
                } else {
                    const res2 = {
                        code: 200,
                        success: true,
                        msg: '操作成功',
                        data: {
                            id: '1744728154160541698',
                            openid: 'oMSM26U-6j-KcKDtKgShaeE0FWeU',
                            userName: '用户名',
                            avatar: '',
                            age: 1,
                            grade: '',
                            sex: '0',
                            phone: '',
                            souceId: '',
                            sourceType: '',
                            acquisitionNum: '',
                            accessToken: 'baae069331a44f5689d838f77bfa59cc'
                        },
                        time: '2024-01-11 22:51:06',
                        traceId: '1745458312874098690'
                    };
                    this.userinfo = res2.data;
                    const openId = this.userinfo.openid + '_data';
                    cookies.set(openId, this.userinfo, '7d');
                    cookies.set('openId', this.userinfo.openid, '7d');
                    console.log('写入缓存成功')
                    resolve();

                    // const router = useRoute();
                    // getCustomer({ code: router.query.code }).then((res: getCustomerValidator.ReturnType): void => {
                    //     this.userinfo = res.data;
                    //     const openId = res.data.openid + '_data';
                    //     cookies.set(openId, res.data, '7d');
                    //     cookies.set('openId', res.data.openid, '7d');
                    //     console.log('写入缓存成功');
                    //     resolve();
                    // });
                }
            });
        }
    }
});
