import { $get, $http, $post } from '@/packages/request';

import type { getListDevValidator } from './validation/getListDev';
import type { getCustomerValidator } from './validation/api';
import type { getBenefitValidator } from './validation/api';

export const getListDev = (params: getListDevValidator.ParamType): Promise<getListDevValidator.ReturnType> => {
    return $post('/mock/14/demo/getList', params, {
        retryDelay: 4000, //当前请求重试间隔设置为4秒
        retryFrequency: 2 //当前请求重试次数（重试频率）2次
    });
};

// 获取用户基本信息
export const getCustomer = (params: getCustomerValidator.ParamType): Promise<getCustomerValidator.ReturnType> => {
    return $get(`/wx/mp/login`, params, {
        retryDelay: 4000, //当前请求重试间隔设置为4秒
        retryFrequency: 2 //当前请求重试次数（重试频率）2次
    });
};

// 用户权益列表
export const getBenefit = (id: string): Promise<getBenefitValidator.ReturnType> => {
    return $post(
        `/mobile/benefitBalance/byCustomerId/${id}`,
        {},
        {
            retryDelay: 4000, //当前请求重试间隔设置为4秒
            retryFrequency: 2 //当前请求重试次数（重试频率）2次
        }
    );
};

// 权益消费记录列表
export const getBenefitRecordList = (obj: {
    customerId: any;
    pageIndex: any;
    pageSize: any;
    benefitId: number;
}): Promise<getBenefitValidator.ReturnType> => {
    return $http(
        `/mobile/benefitRecord/getBenefitRecordList`,
        {
            customerId: obj.customerId,
            pageIndex: obj.pageIndex,
            pageSize: obj.pageSize,
            benefitId: obj.benefitId
        },
        {
            retryDelay: 4000, //当前请求重试间隔设置为4秒
            retryFrequency: 2 //当前请求重试次数（重试频率）2次
        }
    );
};
// 权益获取记录列表
export const getRecordList = (obj: {
    customerId: any;
    pageIndex: any;
    pageSize: any;
    benefitId: number;
}): Promise<getBenefitValidator.ReturnType> => {
    return $http(
        `/mobile/benefitOrder/getBenefitOrderList`,
        {
            customerId: obj.customerId,
            pageIndex: obj.pageIndex,
            pageSize: obj.pageSize,
            benefitId: obj.benefitId
        },
        {
            retryDelay: 4000, //当前请求重试间隔设置为4秒
            retryFrequency: 2 //当前请求重试次数（重试频率）2次
        }
    );
};
export const doctorLogin = (customerId: string) => {
    return $http(
        `/wx/mp/doctor/login?customerId=${customerId}`,
        {},
        {
            retryDelay: 4000, //当前请求重试间隔设置为4秒
            retryFrequency: 2 //当前请求重试次数（重试频率）2次
        }
    );
};

// 用户视频医生权益判断
export const judgement = (customerId: string) => {
    return $http(
        `/mobile/benefitBalance/doctor/judgement/${customerId}`,
        {},
        {
            retryDelay: 4000, //当前请求重试间隔设置为4秒
            retryFrequency: 2 //当前请求重试次数（重试频率）2次
        }
    );
};

// 用户视频医生权益判断
export const authorize = () => {
    return $get(
        `/wx/mp/authorize`,
        {},
        {
            retryDelay: 4000, //当前请求重试间隔设置为4秒
            retryFrequency: 2 //当前请求重试次数（重试频率）2次
        }
    );
};
