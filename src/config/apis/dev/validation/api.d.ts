export namespace getCustomerValidator {
    /** 接口入参类型定义 */
    export interface ParamType {
        code: string;
    }
    /** 接口回参类型定义 */
    export interface Data {
        id: string;
        openid: string;
        userName: string;
        avatar: string;
        age: number;
        grade: string;
        sex: string;
        phone: string;
        souceId: string;
        sourceType: string;
        acquisitionNum: string;
        accessToken: string;
    }
    export interface ReturnType {
        code: string;
        data: Data;
        time: string;
        traceId: string;
        msg: string;
        success: boolean;
    }
}

export namespace getBenefitValidator {
    /** 接口入参类型定义 */
    export interface ParamType {
        code: string;
    }
    /** 接口回参类型定义 */
    export interface Data {
        filter(arg0: (item: { benefitName: string }) => { benefitName: string } | undefined);
        id: string;
        benefitId: string;
        benefitName: string;
        quantity: number;
        expiredDate: number;
    }
    export interface ReturnType {
        code: string;
        data: Data;
        time: string;
        traceId: string;
        msg: string;
        success: boolean;
    }
}
